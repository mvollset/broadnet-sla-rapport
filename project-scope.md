## Mål ##
Målet er å automatisere den månedlige SLA rapporteringen hos Broadnet. 

## Dagens løsning
Dagens løsning er en rekke excel ark som fylles med data fra Remedy rapporter og SQL spørringer, som så prosesseres ved hjelp av excel makroer e.l. for at dataene skal bli komplette. Dette er en ganske tidkrevende og manuell prosess. 

## Løsningsforslag
Vi ønsker å lage en rapport som kan hentes på en nettside hvor brukeren selv skal kunnne velge kunde/site og periode rapporten skal kjøres for. Det vil være mulig å tilgangsstyre rapportene ved hjelp av vanlige windows AD grupper. Resultatet kan så eksporteres til et Excel ark med en fane pr "spørring". 

## Forarbeid
Vi har sammen med Espen sett på hva som ligger av data i dagens rapporter. Sammen med Eirik J. har jeg så gått igjennom hva som er tilgjengelig direkte i Remedy i dag for å finne ut hva som er der i dag. Og hva som mangler, og evt. hva som må gjøres for at det skal være mulig å ta ut. 
Det aller meste er allerede på plass, det eneste som må gjøres på remedy siden er å kalkulere nedetid for alle mulige SLA'er for hver felles feil. Dette har Eirik allerede ha implementert i test. 

## Hva skal rapporten inneholde
Gitt en kunde og en tidsperiode,dato fra og til, skal rapporten inneholde følgende:

### Alle incidents meldt inn på kundens samband i perioden
Følgende felter skal med
 * Incident ID*+
 * Customer Ticket Nr
 * Company
 * (Kundenummer) ?Customer Site?
 * Reported Date+
 * First Name+
 * Last Name+
 * CI+
 * Summary*
 * Notes
 * Resolution
 * Resolution Categorization Tier 1
 * UpTimeChar
 * Status*
 * SLA
 * Service*+
 * SLM Real Time Status
 * Operational Categorization Tier 2
 * Incident Type*
 * Last Resolved Date
 * Resolution Categorization Tier 2
 * Resolution Categorization Tier 3
 * Host Name
 * Kundenummer
 * CI+
 * MRC
 * Redundance CI
 * A-Address
 * SLA Servicetid product
 * SLA Kvalitetsgaranti product
 * Tjeneste product

### Fellesfeil som har berørt kundens samband i perioden
Informasjon som skal med  
 * Sambandsnummer
 * Tid ned
 * Tid opp
 * Total nedetid SLA
 * Forklaring? Hvor skal vi evt. hente dette fra. (Fellesfeil) Resolution fra Incidenten

### Planlagt arbeid på kundens samband i perioden
> Skal dette med selv om det ikke har noe å si for evt. refusjon?

Informasjon som skal med  
 * Sambandsnummer
 * Tid ned
 * Tid opp
 * Total nedetid
 * Forklaring? Hvor skal vi evt. hente dette fra.

### Sammenstilling av fellesfeil og planlagt arbeid.


### Estimat
> I estimatet tar jeg utgangspunkt i "rapporter". En rapport er en databasespørring, en enkel rapport er estimert til 4 timer,en
>avansert til 8. Disse rapportene skal så settes sammen til en sammensatt rapport til slutt. 
 * Installasjon 4 timer
 * Incident liste (Enkel rapport) 4 timer
 * Fellesfeil (Enkel rapport) 4 timer
 * Planlagt arbeid (Enkel rapport) 4 timer
 * Nedetid pr samband (Avansert rapport) 8 timer
 * Excel eksport 8 timer
 * 


### Spørringer
```sql
SELECT DATEADD(s,in_tpl_down,'1970-1-1') down,DATEADD(s,in_tpl_actualUp2,'1970-1-1')up ,t.summary,t.RootRequestID,n.* FROM TMS_Task t
inner join BT_CustomerNotificationSetupv2 n ON (t.RootRequestID=n.Parent_Request_ID__Incident_Ch and Role_x='Alert')
 WHERE t.instanceid in(

select task_instance_id from BT_CircuitLookupResult WHERE CircuitNumber in(
SELECT Name FROm AST_Sambandsnummer where Reconciliation_Identity in(
SELECT AssetInstanceId FROM AST_AssetPeople where PeopleGroup_Form_Entry_ID in(

SELECT People_Organization_ID FROM CTM_People_Organization where company='Newco Holding AS 235140')
)))
and t.RootRequestID like 'INC%'
SELECT DATEADD(s,in_tpl_down,'1970-1-1') down,DATEADD(s,dt_tpl_expectedUp,'1970-1-1')up ,t.summary,t.RootRequestID,n.* FROM TMS_Task t
inner join BT_CustomerNotificationSetupv2 n ON (t.RootRequestID=n.Parent_Request_ID__Incident_Ch and Role_x='Planlagt Arbeid')
 WHERE t.instanceid in(

select task_instance_id from BT_CircuitLookupResult WHERE CircuitNumber in(
SELECT Name FROm AST_Sambandsnummer where Reconciliation_Identity in(
SELECT AssetInstanceId FROM AST_AssetPeople where PeopleGroup_Form_Entry_ID in(

SELECT People_Organization_ID FROM CTM_People_Organization where company='Newco Holding AS 235140')
)))
and t.RootRequestID like 'CRQ%'
```
